# Description
## Dependencies
* dotenv
* express
* mongoose

## Dev Dependencies
* nodemon
* colors
* webpack
* webpack-cli
* webpacl-merge
* webpack-node-externals

## about HTTP status code
* 1.xx : informational

* 2.xx : Success
* 200 : Success
* 201 : Created
* 204 : No Content

* 3.xx : Redirection
* 304 : Not Modified

* 4.xx : Client Error
* 400 : Bad Request
* 401 : Unauthorized
* 404 : Not Found

* 5.xx : Sever Error
* 500 : Internal Server Error