import React, { useEffect, useState } from "react";
import { fetchFolderlist } from "../apis/index";
import FolderItems from "./FolderItems";

const Top = () => {
    const [folderList, setFolderList] = useState("");

    useEffect(() => {
        fetchFolderlist().then((res) => {
            setFolderList(res.data.data);
        });
    }, []);
    console.log(folderList);
    return (
        <div>
            {folderList &&
                folderList.map((folder) => {
                    return (
                        <FolderItems
                            key={folder._id}
                            dirname={folder.dirname}
                            dirpath={folder.dirpath}
                        ></FolderItems>
                    );
                })}
        </div>
    );
};

export default Top;
