import axios from "axios";

const BASE_URL = "http://localhost:5000/api/v1";

const folderapi = axios.create({
    baseURL: BASE_URL,
});

export const fetchFolderlist = async () => {
    return await folderapi.get("/images");
};
