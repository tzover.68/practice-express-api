const express = require("express");
const router = express.Router();
const {
    getUsers,
    getUser,
    createUsers,
    updateUsers,
    deleteUsers,
} = require("../../controllers/users");

router.route("/").get(getUsers).post(createUsers);
router.route("/:id").get(getUser).put(updateUsers).delete(deleteUsers);

module.exports = router;
