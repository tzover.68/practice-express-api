const express = require("express");
const router = express.Router();
const imageRouter = require("./images");
const userRouter = require("./users");

router.use("/images", imageRouter);
router.use("/users", userRouter);

module.exports = router;
