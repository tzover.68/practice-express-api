const express = require("express");
const router = express.Router();
const {
    getImages,
    getImage,
    createImage,
    updateImage,
    deleteImage,
} = require("../../controllers/images");

router.route("/").get(getImages).post(createImage);
router.route("/:id").get(getImage).put(updateImage).delete(deleteImage);

module.exports = router;
