const mongoose = require("mongoose");

const ImageSchema = new mongoose.Schema({
    dirname: {
        type: String,
        required: [true, "Please add a dirname"],
        unique: true,
    },
    dirpath: {
        type: String,
        required: [true, "Please add a dirpath"],
        unique: true,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("Image", ImageSchema);
