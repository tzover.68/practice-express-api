const express = require("express");
const dotenv = require("dotenv");
const router = require("./routes/v1/index");
// const logger = require("./middleware/logger");
const morgan = require("morgan");
const cors = require("cors");
const errorHandler = require("./middleware/error");
const connectDB = require("./config/db");

dotenv.config({ path: "./src/config/config.env" });

connectDB();

const app = express();

app.use(express.json());
// app.use(logger);
if (process.env.NODE_ENV == "develop") {
    app.use(morgan("dev"));
}
app.use(cors());
app.use("/api/v1", router);
app.use(errorHandler);

const PORT = process.env.PORT;

const server = app.listen(PORT, () => {
    console.log(
        `App is running in ${process.env.NODE_ENV} mode and listening on port ${PORT}!`
    );
});

process.on("unhandledRejection", (err, promise) => {
    console.log(`Error: ${err.message}`);
    server.close(() => process.exit(1));
});
