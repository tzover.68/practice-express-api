const Image = require("../models/Image");
const ErrorResponse = require("../utils/erroResponse");
const asyncHandler = require("../middleware/async");

exports.getImages = asyncHandler(async (req, res, next) => {
    const images = await Image.find();
    res.status(200).json({
        success: true,
        count: images.length,
        data: images,
    });
});

exports.getImage = asyncHandler(async (req, res, next) => {
    const image = await Image.findById(req.params.id);
    res.status(200).json({
        success: true,
        data: image,
    });
});

exports.createImage = asyncHandler(async (req, res, next) => {
    const image = await Image.create(req.body);
    res.status(201).json({
        success: true,
        data: image,
    });
});

exports.updateImage = asyncHandler(async (req, res, next) => {
    const image = await Image.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });

    if (!image) {
        return res.status(400).json({
            success: false,
        });
    }

    res.status(200).json({
        success: true,
        data: image,
    });
});

exports.deleteImage = asyncHandler(async (req, res, next) => {
    const image = await Image.findByIdAndDelete(req.params.id);

    if (!image) {
        return res.status(400).json({
            success: false,
        });
    }

    res.status(200).json({
        success: true,
        data: {},
    });
});
