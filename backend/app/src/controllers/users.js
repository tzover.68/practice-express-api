exports.getUsers = (req, res, next) => {
    res.status(200).json({
        success: true,
        msg: "Show all Users",
        data: {
            id: 1,
        },
    });
};

exports.getUser = (req, res, next) => {
    res.status(200).json({
        success: true,
        msg: `Show User ${req.params.id}`,
        data: {
            id: 1,
        },
    });
};

exports.createUsers = (req, res, next) => {
    res.status(200).json({
        success: true,
        msg: "Create a new User",
        data: {
            id: 1,
        },
    });
};

exports.updateUsers = (req, res, next) => {
    res.status(200).json({
        success: true,
        msg: `Update User ${req.params.id}`,
        data: {
            id: 1,
        },
    });
};

exports.deleteUsers = (req, res, next) => {
    res.status(200).json({
        success: true,
        msg: `Delete User ${req.params.id}`,
        data: {
            id: 1,
        },
    });
};
